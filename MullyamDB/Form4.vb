﻿Public Class Form4

    Private Sub StudentBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles StudentBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.StudentBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.MullyamDataSet)

    End Sub

    Private Sub Form4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.StudGroup". При необходимости она может быть перемещена или удалена.
        Me.StudGroupTableAdapter.Fill(Me.MullyamDataSet.StudGroup)
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.Student". При необходимости она может быть перемещена или удалена.
        Me.StudentTableAdapter.Fill(Me.MullyamDataSet.Student)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Form6.Show()

    End Sub
End Class