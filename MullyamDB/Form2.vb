﻿Public Class Form2

    Private Sub StudGroupBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles StudGroupBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.StudGroupBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.MullyamDataSet)

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.Student". При необходимости она может быть перемещена или удалена.
        Me.StudentTableAdapter.Fill(Me.MullyamDataSet.Student)
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.StudGroup". При необходимости она может быть перемещена или удалена.
        Me.StudGroupTableAdapter.Fill(Me.MullyamDataSet.StudGroup)

    End Sub
End Class