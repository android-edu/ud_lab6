﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form3))
        Dim Id_LectLabel As System.Windows.Forms.Label
        Dim FIOLabel As System.Windows.Forms.Label
        Dim StageLabel As System.Windows.Forms.Label
        Dim KafedraLabel As System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MullyamDataSet = New MullyamDB.MullyamDataSet()
        Me.LecturerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LecturerTableAdapter = New MullyamDB.MullyamDataSetTableAdapters.LecturerTableAdapter()
        Me.TableAdapterManager = New MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager()
        Me.LecturerBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.LecturerBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Id_LectTextBox = New System.Windows.Forms.TextBox()
        Me.FIOTextBox = New System.Windows.Forms.TextBox()
        Me.StageTextBox = New System.Windows.Forms.TextBox()
        Me.KafedraTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Id_LectLabel = New System.Windows.Forms.Label()
        FIOLabel = New System.Windows.Forms.Label()
        StageLabel = New System.Windows.Forms.Label()
        KafedraLabel = New System.Windows.Forms.Label()
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LecturerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LecturerBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LecturerBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(79, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Таблица ""Лектор"""
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MullyamDataSet
        '
        Me.MullyamDataSet.DataSetName = "MullyamDataSet"
        Me.MullyamDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LecturerBindingSource
        '
        Me.LecturerBindingSource.DataMember = "Lecturer"
        Me.LecturerBindingSource.DataSource = Me.MullyamDataSet
        '
        'LecturerTableAdapter
        '
        Me.LecturerTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ExamQTableAdapter = Nothing
        Me.TableAdapterManager.ExamTableAdapter = Nothing
        Me.TableAdapterManager.LecturerQTableAdapter = Nothing
        Me.TableAdapterManager.LecturerTableAdapter = Me.LecturerTableAdapter
        Me.TableAdapterManager.StudentQTableAdapter = Nothing
        Me.TableAdapterManager.StudentTableAdapter = Nothing
        Me.TableAdapterManager.StudGroupQTableAdapter = Nothing
        Me.TableAdapterManager.StudGroupTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'LecturerBindingNavigator
        '
        Me.LecturerBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.LecturerBindingNavigator.BindingSource = Me.LecturerBindingSource
        Me.LecturerBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.LecturerBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.LecturerBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.LecturerBindingNavigatorSaveItem})
        Me.LecturerBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.LecturerBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.LecturerBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.LecturerBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.LecturerBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.LecturerBindingNavigator.Name = "LecturerBindingNavigator"
        Me.LecturerBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.LecturerBindingNavigator.Size = New System.Drawing.Size(399, 27)
        Me.LecturerBindingNavigator.TabIndex = 1
        Me.LecturerBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Переместить в начало"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Переместить назад"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Положение"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Текущее положение"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(55, 20)
        Me.BindingNavigatorCountItem.Text = "для {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Общее число элементов"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Переместить вперед"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Переместить в конец"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 24)
        Me.BindingNavigatorAddNewItem.Text = "Добавить"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Удалить"
        '
        'LecturerBindingNavigatorSaveItem
        '
        Me.LecturerBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.LecturerBindingNavigatorSaveItem.Image = CType(resources.GetObject("LecturerBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.LecturerBindingNavigatorSaveItem.Name = "LecturerBindingNavigatorSaveItem"
        Me.LecturerBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 23)
        Me.LecturerBindingNavigatorSaveItem.Text = "Сохранить данные"
        '
        'Id_LectLabel
        '
        Id_LectLabel.AutoSize = True
        Id_LectLabel.Location = New System.Drawing.Point(89, 90)
        Id_LectLabel.Name = "Id_LectLabel"
        Id_LectLabel.Size = New System.Drawing.Size(54, 17)
        Id_LectLabel.TabIndex = 2
        Id_LectLabel.Text = "Id Lect:"
        '
        'Id_LectTextBox
        '
        Me.Id_LectTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LecturerBindingSource, "Id_Lect", True))
        Me.Id_LectTextBox.Location = New System.Drawing.Point(149, 87)
        Me.Id_LectTextBox.Name = "Id_LectTextBox"
        Me.Id_LectTextBox.Size = New System.Drawing.Size(208, 22)
        Me.Id_LectTextBox.TabIndex = 3
        '
        'FIOLabel
        '
        FIOLabel.AutoSize = True
        FIOLabel.Location = New System.Drawing.Point(109, 118)
        FIOLabel.Name = "FIOLabel"
        FIOLabel.Size = New System.Drawing.Size(34, 17)
        FIOLabel.TabIndex = 4
        FIOLabel.Text = "FIO:"
        '
        'FIOTextBox
        '
        Me.FIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LecturerBindingSource, "FIO", True))
        Me.FIOTextBox.Location = New System.Drawing.Point(149, 115)
        Me.FIOTextBox.Name = "FIOTextBox"
        Me.FIOTextBox.Size = New System.Drawing.Size(208, 22)
        Me.FIOTextBox.TabIndex = 5
        '
        'StageLabel
        '
        StageLabel.AutoSize = True
        StageLabel.Location = New System.Drawing.Point(94, 146)
        StageLabel.Name = "StageLabel"
        StageLabel.Size = New System.Drawing.Size(49, 17)
        StageLabel.TabIndex = 6
        StageLabel.Text = "Stage:"
        '
        'StageTextBox
        '
        Me.StageTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LecturerBindingSource, "Stage", True))
        Me.StageTextBox.Location = New System.Drawing.Point(149, 143)
        Me.StageTextBox.Name = "StageTextBox"
        Me.StageTextBox.Size = New System.Drawing.Size(208, 22)
        Me.StageTextBox.TabIndex = 7
        '
        'KafedraLabel
        '
        KafedraLabel.AutoSize = True
        KafedraLabel.Location = New System.Drawing.Point(81, 174)
        KafedraLabel.Name = "KafedraLabel"
        KafedraLabel.Size = New System.Drawing.Size(62, 17)
        KafedraLabel.TabIndex = 8
        KafedraLabel.Text = "Kafedra:"
        '
        'KafedraTextBox
        '
        Me.KafedraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LecturerBindingSource, "Kafedra", True))
        Me.KafedraTextBox.Location = New System.Drawing.Point(149, 171)
        Me.KafedraTextBox.Name = "KafedraTextBox"
        Me.KafedraTextBox.Size = New System.Drawing.Size(208, 22)
        Me.KafedraTextBox.TabIndex = 9
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(112, 263)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(161, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Индивидуальное 3"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 376)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(KafedraLabel)
        Me.Controls.Add(Me.KafedraTextBox)
        Me.Controls.Add(StageLabel)
        Me.Controls.Add(Me.StageTextBox)
        Me.Controls.Add(FIOLabel)
        Me.Controls.Add(Me.FIOTextBox)
        Me.Controls.Add(Id_LectLabel)
        Me.Controls.Add(Me.Id_LectTextBox)
        Me.Controls.Add(Me.LecturerBindingNavigator)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form3"
        Me.Text = "Таблица ""Лектор"""
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LecturerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LecturerBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LecturerBindingNavigator.ResumeLayout(False)
        Me.LecturerBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MullyamDataSet As MullyamDB.MullyamDataSet
    Friend WithEvents LecturerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LecturerTableAdapter As MullyamDB.MullyamDataSetTableAdapters.LecturerTableAdapter
    Friend WithEvents TableAdapterManager As MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager
    Friend WithEvents LecturerBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LecturerBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Id_LectTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StageTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KafedraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
