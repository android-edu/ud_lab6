﻿Public Class Form5

    Private Sub ExamBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles ExamBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.ExamBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.MullyamDataSet)

    End Sub

    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.Lecturer". При необходимости она может быть перемещена или удалена.
        Me.LecturerTableAdapter.Fill(Me.MullyamDataSet.Lecturer)
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.Exam". При необходимости она может быть перемещена или удалена.
        Me.ExamTableAdapter.Fill(Me.MullyamDataSet.Exam)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ExamBindingSource.MoveFirst()

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        ExamBindingSource.MoveLast()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ExamBindingSource.MovePrevious()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        ExamBindingSource.MoveNext()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ExamBindingSource.AddNew()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ExamBindingSource.RemoveCurrent()
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Me.Validate()
        Me.ExamBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.MullyamDataSet)
    End Sub
End Class