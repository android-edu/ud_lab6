﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form5
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form5))
        Dim Id_StudentLabel As System.Windows.Forms.Label
        Dim SubjectLabel As System.Windows.Forms.Label
        Dim MarkLabel As System.Windows.Forms.Label
        Dim Exam_DateLabel As System.Windows.Forms.Label
        Dim Id_LectLabel As System.Windows.Forms.Label
        Me.MullyamDataSet = New MullyamDB.MullyamDataSet()
        Me.ExamBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ExamTableAdapter = New MullyamDB.MullyamDataSetTableAdapters.ExamTableAdapter()
        Me.TableAdapterManager = New MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager()
        Me.ExamBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ExamBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.SubjectTextBox = New System.Windows.Forms.TextBox()
        Me.MarkTextBox = New System.Windows.Forms.TextBox()
        Me.Exam_DateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MullyamDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ExamBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.LecturerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LecturerTableAdapter = New MullyamDB.MullyamDataSetTableAdapters.LecturerTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Id_StudentLabel = New System.Windows.Forms.Label()
        SubjectLabel = New System.Windows.Forms.Label()
        MarkLabel = New System.Windows.Forms.Label()
        Exam_DateLabel = New System.Windows.Forms.Label()
        Id_LectLabel = New System.Windows.Forms.Label()
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExamBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExamBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExamBindingNavigator.SuspendLayout()
        CType(Me.MullyamDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExamBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LecturerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MullyamDataSet
        '
        Me.MullyamDataSet.DataSetName = "MullyamDataSet"
        Me.MullyamDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ExamBindingSource
        '
        Me.ExamBindingSource.DataMember = "Exam"
        Me.ExamBindingSource.DataSource = Me.MullyamDataSet
        '
        'ExamTableAdapter
        '
        Me.ExamTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ExamQTableAdapter = Nothing
        Me.TableAdapterManager.ExamTableAdapter = Me.ExamTableAdapter
        Me.TableAdapterManager.LecturerQTableAdapter = Nothing
        Me.TableAdapterManager.LecturerTableAdapter = Me.LecturerTableAdapter
        Me.TableAdapterManager.StudentQTableAdapter = Nothing
        Me.TableAdapterManager.StudentTableAdapter = Nothing
        Me.TableAdapterManager.StudGroupQTableAdapter = Nothing
        Me.TableAdapterManager.StudGroupTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ExamBindingNavigator
        '
        Me.ExamBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.ExamBindingNavigator.BindingSource = Me.ExamBindingSource
        Me.ExamBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ExamBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ExamBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.ExamBindingNavigatorSaveItem})
        Me.ExamBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ExamBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ExamBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ExamBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ExamBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ExamBindingNavigator.Name = "ExamBindingNavigator"
        Me.ExamBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ExamBindingNavigator.Size = New System.Drawing.Size(484, 27)
        Me.ExamBindingNavigator.TabIndex = 0
        Me.ExamBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Переместить в начало"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Переместить назад"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Положение"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Текущее положение"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(55, 20)
        Me.BindingNavigatorCountItem.Text = "для {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Общее число элементов"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Переместить вперед"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Переместить в конец"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 24)
        Me.BindingNavigatorAddNewItem.Text = "Добавить"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Удалить"
        '
        'ExamBindingNavigatorSaveItem
        '
        Me.ExamBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ExamBindingNavigatorSaveItem.Image = CType(resources.GetObject("ExamBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ExamBindingNavigatorSaveItem.Name = "ExamBindingNavigatorSaveItem"
        Me.ExamBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 23)
        Me.ExamBindingNavigatorSaveItem.Text = "Сохранить данные"
        '
        'Id_StudentLabel
        '
        Id_StudentLabel.AutoSize = True
        Id_StudentLabel.Location = New System.Drawing.Point(66, 125)
        Id_StudentLabel.Name = "Id_StudentLabel"
        Id_StudentLabel.Size = New System.Drawing.Size(76, 17)
        Id_StudentLabel.TabIndex = 1
        Id_StudentLabel.Text = "Id Student:"
        '
        'SubjectLabel
        '
        SubjectLabel.AutoSize = True
        SubjectLabel.Location = New System.Drawing.Point(83, 153)
        SubjectLabel.Name = "SubjectLabel"
        SubjectLabel.Size = New System.Drawing.Size(59, 17)
        SubjectLabel.TabIndex = 3
        SubjectLabel.Text = "Subject:"
        '
        'SubjectTextBox
        '
        Me.SubjectTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ExamBindingSource, "Subject", True))
        Me.SubjectTextBox.Location = New System.Drawing.Point(148, 150)
        Me.SubjectTextBox.Name = "SubjectTextBox"
        Me.SubjectTextBox.Size = New System.Drawing.Size(278, 22)
        Me.SubjectTextBox.TabIndex = 4
        '
        'MarkLabel
        '
        MarkLabel.AutoSize = True
        MarkLabel.Location = New System.Drawing.Point(99, 181)
        MarkLabel.Name = "MarkLabel"
        MarkLabel.Size = New System.Drawing.Size(43, 17)
        MarkLabel.TabIndex = 5
        MarkLabel.Text = "Mark:"
        '
        'MarkTextBox
        '
        Me.MarkTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ExamBindingSource, "Mark", True))
        Me.MarkTextBox.Location = New System.Drawing.Point(148, 178)
        Me.MarkTextBox.Name = "MarkTextBox"
        Me.MarkTextBox.Size = New System.Drawing.Size(278, 22)
        Me.MarkTextBox.TabIndex = 6
        '
        'Exam_DateLabel
        '
        Exam_DateLabel.AutoSize = True
        Exam_DateLabel.Location = New System.Drawing.Point(62, 210)
        Exam_DateLabel.Name = "Exam_DateLabel"
        Exam_DateLabel.Size = New System.Drawing.Size(80, 17)
        Exam_DateLabel.TabIndex = 7
        Exam_DateLabel.Text = "Exam Date:"
        '
        'Exam_DateDateTimePicker
        '
        Me.Exam_DateDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ExamBindingSource, "Exam_Date", True))
        Me.Exam_DateDateTimePicker.Location = New System.Drawing.Point(148, 206)
        Me.Exam_DateDateTimePicker.Name = "Exam_DateDateTimePicker"
        Me.Exam_DateDateTimePicker.Size = New System.Drawing.Size(278, 22)
        Me.Exam_DateDateTimePicker.TabIndex = 8
        '
        'Id_LectLabel
        '
        Id_LectLabel.AutoSize = True
        Id_LectLabel.Location = New System.Drawing.Point(88, 237)
        Id_LectLabel.Name = "Id_LectLabel"
        Id_LectLabel.Size = New System.Drawing.Size(54, 17)
        Id_LectLabel.TabIndex = 9
        Id_LectLabel.Text = "Id Lect:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(191, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 17)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Таблица ""Экзамен"""
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ExamBindingSource, "Id_Student", True))
        Me.ComboBox1.DataSource = Me.ExamBindingSource1
        Me.ComboBox1.DisplayMember = "Id_Student"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(148, 120)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(278, 24)
        Me.ComboBox1.TabIndex = 12
        Me.ComboBox1.ValueMember = "Id_Student"
        '
        'MullyamDataSetBindingSource
        '
        Me.MullyamDataSetBindingSource.DataSource = Me.MullyamDataSet
        Me.MullyamDataSetBindingSource.Position = 0
        '
        'ExamBindingSource1
        '
        Me.ExamBindingSource1.DataMember = "Exam"
        Me.ExamBindingSource1.DataSource = Me.MullyamDataSetBindingSource
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ExamBindingSource, "Id_Student", True))
        Me.ComboBox2.DataSource = Me.LecturerBindingSource
        Me.ComboBox2.DisplayMember = "FIO"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(148, 234)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(278, 24)
        Me.ComboBox2.TabIndex = 13
        Me.ComboBox2.ValueMember = "Id_Lect"
        '
        'LecturerBindingSource
        '
        Me.LecturerBindingSource.DataMember = "Lecturer"
        Me.LecturerBindingSource.DataSource = Me.MullyamDataSetBindingSource
        '
        'LecturerTableAdapter
        '
        Me.LecturerTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(25, 313)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(115, 23)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Первая"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(148, 313)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(131, 23)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "Предыдущая"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(285, 313)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(115, 23)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "Добавить"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(285, 342)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(115, 23)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "Удалить"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(148, 342)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(131, 23)
        Me.Button5.TabIndex = 18
        Me.Button5.Text = "Следующая"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(25, 342)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(115, 23)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "Последняя"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(148, 371)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(131, 23)
        Me.Button7.TabIndex = 20
        Me.Button7.Text = "Сохранить"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Form5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 507)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Id_LectLabel)
        Me.Controls.Add(Exam_DateLabel)
        Me.Controls.Add(Me.Exam_DateDateTimePicker)
        Me.Controls.Add(MarkLabel)
        Me.Controls.Add(Me.MarkTextBox)
        Me.Controls.Add(SubjectLabel)
        Me.Controls.Add(Me.SubjectTextBox)
        Me.Controls.Add(Id_StudentLabel)
        Me.Controls.Add(Me.ExamBindingNavigator)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form5"
        Me.Text = "Таблица ""Экзамен"""
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExamBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExamBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExamBindingNavigator.ResumeLayout(False)
        Me.ExamBindingNavigator.PerformLayout()
        CType(Me.MullyamDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExamBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LecturerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MullyamDataSet As MullyamDB.MullyamDataSet
    Friend WithEvents ExamBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ExamTableAdapter As MullyamDB.MullyamDataSetTableAdapters.ExamTableAdapter
    Friend WithEvents TableAdapterManager As MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ExamBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExamBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents SubjectTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MarkTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Exam_DateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ExamBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MullyamDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LecturerTableAdapter As MullyamDB.MullyamDataSetTableAdapters.LecturerTableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents LecturerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
End Class
