﻿Public Class Form7

    Private Sub LecturerBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles LecturerBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.LecturerBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.MullyamDataSet)

    End Sub

    Private Sub Form7_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "MullyamDataSet.Lecturer". При необходимости она может быть перемещена или удалена.
        Me.LecturerTableAdapter.Fill(Me.MullyamDataSet.Lecturer)

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        ComboBox1.Enabled = CheckBox1.Enabled
        NumericUpDown1.Enabled = CheckBox1.Enabled
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        TextBox1.Enabled = CheckBox2.Enabled
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim str As String
        str = ""

        If CheckBox1.Checked = True Then
            str += "Stage " + ComboBox1.Text + NumericUpDown1.Value.ToString
        End If

        If CheckBox2.Checked = True Then
            If CheckBox1.Checked = True Then
                str = "(" + str + ") AND ("
            End If

            str += "Kafedra = '" + TextBox1.Text + "'"

            If CheckBox1.Checked = True Then
                str += ")"
            End If

        End If

        LecturerBindingSource.Filter = str

    End Sub
End Class