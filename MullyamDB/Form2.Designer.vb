﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Dim NameGroupLabel As System.Windows.Forms.Label
        Dim KafedraLabel As System.Windows.Forms.Label
        Dim KursLabel1 As System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MullyamDataSet = New MullyamDB.MullyamDataSet()
        Me.StudGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StudGroupTableAdapter = New MullyamDB.MullyamDataSetTableAdapters.StudGroupTableAdapter()
        Me.TableAdapterManager = New MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager()
        Me.StudGroupBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.StudGroupBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NameGroupTextBox = New System.Windows.Forms.TextBox()
        Me.KafedraTextBox = New System.Windows.Forms.TextBox()
        Me.KursNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.StudentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StudentTableAdapter = New MullyamDB.MullyamDataSetTableAdapters.StudentTableAdapter()
        Me.StudentDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        NameGroupLabel = New System.Windows.Forms.Label()
        KafedraLabel = New System.Windows.Forms.Label()
        KursLabel1 = New System.Windows.Forms.Label()
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudGroupBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StudGroupBindingNavigator.SuspendLayout()
        CType(Me.KursNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudentDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(35, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(235, 67)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Таблица ""Группа"""
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MullyamDataSet
        '
        Me.MullyamDataSet.DataSetName = "MullyamDataSet"
        Me.MullyamDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'StudGroupBindingSource
        '
        Me.StudGroupBindingSource.DataMember = "StudGroup"
        Me.StudGroupBindingSource.DataSource = Me.MullyamDataSet
        '
        'StudGroupTableAdapter
        '
        Me.StudGroupTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ExamQTableAdapter = Nothing
        Me.TableAdapterManager.ExamTableAdapter = Nothing
        Me.TableAdapterManager.LecturerQTableAdapter = Nothing
        Me.TableAdapterManager.LecturerTableAdapter = Nothing
        Me.TableAdapterManager.StudentQTableAdapter = Nothing
        Me.TableAdapterManager.StudentTableAdapter = Me.StudentTableAdapter
        Me.TableAdapterManager.StudGroupQTableAdapter = Nothing
        Me.TableAdapterManager.StudGroupTableAdapter = Me.StudGroupTableAdapter
        Me.TableAdapterManager.UpdateOrder = MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'StudGroupBindingNavigator
        '
        Me.StudGroupBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.StudGroupBindingNavigator.BindingSource = Me.StudGroupBindingSource
        Me.StudGroupBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.StudGroupBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.StudGroupBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.StudGroupBindingNavigatorSaveItem})
        Me.StudGroupBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.StudGroupBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.StudGroupBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.StudGroupBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.StudGroupBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.StudGroupBindingNavigator.Name = "StudGroupBindingNavigator"
        Me.StudGroupBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.StudGroupBindingNavigator.Size = New System.Drawing.Size(428, 27)
        Me.StudGroupBindingNavigator.TabIndex = 1
        Me.StudGroupBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Переместить в начало"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Переместить назад"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Положение"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Текущее положение"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(55, 20)
        Me.BindingNavigatorCountItem.Text = "для {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Общее число элементов"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Переместить вперед"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Переместить в конец"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 24)
        Me.BindingNavigatorAddNewItem.Text = "Добавить"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Удалить"
        '
        'StudGroupBindingNavigatorSaveItem
        '
        Me.StudGroupBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.StudGroupBindingNavigatorSaveItem.Image = CType(resources.GetObject("StudGroupBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.StudGroupBindingNavigatorSaveItem.Name = "StudGroupBindingNavigatorSaveItem"
        Me.StudGroupBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 23)
        Me.StudGroupBindingNavigatorSaveItem.Text = "Сохранить данные"
        '
        'NameGroupLabel
        '
        NameGroupLabel.AutoSize = True
        NameGroupLabel.Location = New System.Drawing.Point(57, 114)
        NameGroupLabel.Name = "NameGroupLabel"
        NameGroupLabel.Size = New System.Drawing.Size(93, 17)
        NameGroupLabel.TabIndex = 2
        NameGroupLabel.Text = "Name Group:"
        '
        'NameGroupTextBox
        '
        Me.NameGroupTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StudGroupBindingSource, "NameGroup", True))
        Me.NameGroupTextBox.Location = New System.Drawing.Point(156, 111)
        Me.NameGroupTextBox.Name = "NameGroupTextBox"
        Me.NameGroupTextBox.Size = New System.Drawing.Size(175, 22)
        Me.NameGroupTextBox.TabIndex = 3
        '
        'KafedraLabel
        '
        KafedraLabel.AutoSize = True
        KafedraLabel.Location = New System.Drawing.Point(88, 170)
        KafedraLabel.Name = "KafedraLabel"
        KafedraLabel.Size = New System.Drawing.Size(62, 17)
        KafedraLabel.TabIndex = 6
        KafedraLabel.Text = "Kafedra:"
        '
        'KafedraTextBox
        '
        Me.KafedraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StudGroupBindingSource, "Kafedra", True))
        Me.KafedraTextBox.Location = New System.Drawing.Point(156, 167)
        Me.KafedraTextBox.Name = "KafedraTextBox"
        Me.KafedraTextBox.Size = New System.Drawing.Size(175, 22)
        Me.KafedraTextBox.TabIndex = 7
        '
        'KursLabel1
        '
        KursLabel1.AutoSize = True
        KursLabel1.Location = New System.Drawing.Point(109, 139)
        KursLabel1.Name = "KursLabel1"
        KursLabel1.Size = New System.Drawing.Size(41, 17)
        KursLabel1.TabIndex = 8
        KursLabel1.Text = "Kurs:"
        '
        'KursNumericUpDown
        '
        Me.KursNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.StudGroupBindingSource, "Kurs", True))
        Me.KursNumericUpDown.Location = New System.Drawing.Point(156, 139)
        Me.KursNumericUpDown.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.KursNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.KursNumericUpDown.Name = "KursNumericUpDown"
        Me.KursNumericUpDown.Size = New System.Drawing.Size(195, 22)
        Me.KursNumericUpDown.TabIndex = 9
        Me.KursNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'StudentBindingSource
        '
        Me.StudentBindingSource.DataMember = "FK_Student_StudGroup"
        Me.StudentBindingSource.DataSource = Me.StudGroupBindingSource
        '
        'StudentTableAdapter
        '
        Me.StudentTableAdapter.ClearBeforeFill = True
        '
        'StudentDataGridView
        '
        Me.StudentDataGridView.AutoGenerateColumns = False
        Me.StudentDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.StudentDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.StudentDataGridView.DataSource = Me.StudentBindingSource
        Me.StudentDataGridView.Location = New System.Drawing.Point(31, 221)
        Me.StudentDataGridView.Name = "StudentDataGridView"
        Me.StudentDataGridView.RowTemplate.Height = 24
        Me.StudentDataGridView.Size = New System.Drawing.Size(385, 220)
        Me.StudentDataGridView.TabIndex = 9
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Id_Student"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Номер"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "FIO"
        Me.DataGridViewTextBoxColumn2.HeaderText = "ФИО"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(428, 494)
        Me.Controls.Add(Me.StudentDataGridView)
        Me.Controls.Add(KursLabel1)
        Me.Controls.Add(Me.KursNumericUpDown)
        Me.Controls.Add(KafedraLabel)
        Me.Controls.Add(Me.KafedraTextBox)
        Me.Controls.Add(NameGroupLabel)
        Me.Controls.Add(Me.NameGroupTextBox)
        Me.Controls.Add(Me.StudGroupBindingNavigator)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form2"
        Me.Text = "Таблица ""Группа"""
        CType(Me.MullyamDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudGroupBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StudGroupBindingNavigator.ResumeLayout(False)
        Me.StudGroupBindingNavigator.PerformLayout()
        CType(Me.KursNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudentDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MullyamDataSet As MullyamDB.MullyamDataSet
    Friend WithEvents StudGroupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StudGroupTableAdapter As MullyamDB.MullyamDataSetTableAdapters.StudGroupTableAdapter
    Friend WithEvents TableAdapterManager As MullyamDB.MullyamDataSetTableAdapters.TableAdapterManager
    Friend WithEvents StudGroupBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StudGroupBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NameGroupTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KafedraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KursNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents StudentTableAdapter As MullyamDB.MullyamDataSetTableAdapters.StudentTableAdapter
    Friend WithEvents StudentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StudentDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
